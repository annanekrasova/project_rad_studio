//---------------------------------------------------------------------------

#ifndef dmudatabaseH
#define dmudatabaseH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <DataSnap.DSServer.hpp>
#include <Datasnap.DSProviderDataModuleAdapter.hpp>
//---------------------------------------------------------------------------
class Tdmdatabase : public TDSServerModule
{
__published:	// IDE-managed Components
private:	// User declarations
public:		// User declarations
	__fastcall Tdmdatabase(TComponent* Owner);
	System::UnicodeString EchoString(System::UnicodeString value);
	System::UnicodeString  ReverseString(System::UnicodeString value);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdmdatabase *dmDatabase;
//---------------------------------------------------------------------------
#endif

