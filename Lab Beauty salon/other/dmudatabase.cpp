//---------------------------------------------------------------------------

#pragma hdrstop

#include "dmudatabase.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall Tdmdatabase::Tdmdatabase(TComponent* Owner)
	: TDSServerModule(Owner)
{
}
//----------------------------------------------------------------------------
System::UnicodeString Tdmdatabase::EchoString(System::UnicodeString value)
{
    return value;
}
//----------------------------------------------------------------------------
System::UnicodeString Tdmdatabase::ReverseString(System::UnicodeString value)
{
    return ::ReverseString(value);
}
//----------------------------------------------------------------------------

