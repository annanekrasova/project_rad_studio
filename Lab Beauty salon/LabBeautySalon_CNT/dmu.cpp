//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "dmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
TClientModule1 *ClientModule1;
//---------------------------------------------------------------------------
__fastcall TClientModule1::TClientModule1(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall TClientModule1::~TClientModule1()
{
	delete FdmdatabaseClient;
}

TdmdatabaseClient* TClientModule1::GetdmdatabaseClient(void)
{
	if (FdmdatabaseClient == NULL)
	{
		SQLConnection1->Open();
		FdmdatabaseClient = new TdmdatabaseClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmdatabaseClient;
};

void __fastcall TClientModule1::cdsCategoryAfterScroll(TDataSet *DataSet)
{      cdsService->Filter =
	 cdsServiceCATEGORY_ID->FieldName + " = " +cdsCategoryID->AsString;
}
//---------------------------------------------------------------------------
void __fastcall TClientModule1::cdsCategoryAfterPost(TDataSet *DataSet)
{
	  cdsCategory->ApplyUpdates(-1);
}
//---------------------------------------------------------------------------
void __fastcall TClientModule1::cdsServiceAfterPost(TDataSet *DataSet)
{
       cdsService->ApplyUpdates(-1);
}
//---------------------------------------------------------------------------
void __fastcall TClientModule1::DataModuleCreate(TObject *Sender)
{
	cdsCategory->Open();
	cdsService->Open();
}
//---------------------------------------------------------------------------
