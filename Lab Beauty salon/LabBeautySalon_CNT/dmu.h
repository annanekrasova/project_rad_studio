//----------------------------------------------------------------------------

#ifndef dmuH
#define dmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "uproxy.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
//----------------------------------------------------------------------------
class TClientModule1 : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *cdsService;
	TClientDataSet *cdsCategory;
	TIntegerField *cdsServiceID;
	TIntegerField *cdsServiceCATEGORY_ID;
	TWideStringField *cdsServiceNAME;
	TSingleField *cdsServicePRICE;
	TIntegerField *cdsServiceDURATION;
	TIntegerField *cdsCategoryID;
	TWideStringField *cdsCategoryNAME;
	void __fastcall cdsCategoryAfterScroll(TDataSet *DataSet);
	void __fastcall cdsCategoryAfterPost(TDataSet *DataSet);
	void __fastcall cdsServiceAfterPost(TDataSet *DataSet);
	void __fastcall DataModuleCreate(TObject *Sender);
private:	// User declarations
	bool FInstanceOwner;
	TdmdatabaseClient* FdmdatabaseClient;
	TdmdatabaseClient* GetdmdatabaseClient(void);
public:		// User declarations
	__fastcall TClientModule1(TComponent* Owner);
	__fastcall ~TClientModule1();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmdatabaseClient* dmdatabaseClient = {read=GetdmdatabaseClient, write=FdmdatabaseClient};
};
//---------------------------------------------------------------------------
extern PACKAGE TClientModule1 *ClientModule1;
//---------------------------------------------------------------------------
#endif
