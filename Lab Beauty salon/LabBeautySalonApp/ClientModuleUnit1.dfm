object dm: Tdm
  OldCreateOrder = False
  Height = 271
  Width = 415
  object SQLConnection1: TSQLConnection
    DriverName = 'DataSnap'
    LoginPrompt = False
    Params.Strings = (
      'Port=211'
      'CommunicationProtocol=tcp/ip'
      'DatasnapContext=datasnap/')
    Connected = True
    Left = 48
    Top = 40
  end
  object DSProviderConnection1: TDSProviderConnection
    ServerClassName = 'Tdmdatabase'
    Connected = True
    SQLConnection = SQLConnection1
    Left = 48
    Top = 112
  end
  object csdCategory: TClientDataSet
    Active = True
    Aggregates = <>
    Params = <>
    ProviderName = 'dspCategory'
    RemoteServer = DSProviderConnection1
    Left = 48
    Top = 176
    object csdCategoryID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object csdCategoryNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 200
    end
  end
  object csdService: TClientDataSet
    Active = True
    Aggregates = <>
    Filtered = True
    Params = <>
    ProviderName = 'dspService'
    RemoteServer = DSProviderConnection1
    Left = 120
    Top = 176
    object csdServiceID: TIntegerField
      FieldName = 'ID'
      ProviderFlags = [pfInUpdate, pfInWhere, pfInKey]
      Required = True
    end
    object csdServiceCATEGORY_ID: TIntegerField
      FieldName = 'CATEGORY_ID'
      Required = True
    end
    object csdServiceNAME: TWideStringField
      FieldName = 'NAME'
      Required = True
      Size = 240
    end
    object csdServicePRICE: TSingleField
      FieldName = 'PRICE'
    end
    object csdServiceDURATION: TIntegerField
      FieldName = 'DURATION'
    end
  end
end
