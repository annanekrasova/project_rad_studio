//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "ClientClassesUnit1.h"
#include "ClientModuleUnit1.h"
#include "Project1PCH1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	tc->GotoVisibleTab (tiCategory->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	tc->GotoVisibleTab (tiService->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
	ShowMessage("�������! ������ � ���� �������� ��� ��������!");
	tc->GotoVisibleTab(tiCategory->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvCategoryItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab(tiService->Index);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::lvServiceItemClick(TObject * const Sender, TListViewItem * const AItem)

{
	tc->GotoVisibleTab(tiService->Index);
}
//---------------------------------------------------------------------------
