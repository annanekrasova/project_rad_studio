//----------------------------------------------------------------------------

#ifndef ClientModuleUnit1H
#define ClientModuleUnit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include "ClientClassesUnit1.h"
#include <Data.DB.hpp>
#include <Data.DBXCommon.hpp>
#include <Data.DBXDataSnap.hpp>
#include <Data.SqlExpr.hpp>
#include <IPPeerClient.hpp>
#include <Datasnap.DBClient.hpp>
#include <Datasnap.DSConnect.hpp>
//----------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TSQLConnection *SQLConnection1;
	TDSProviderConnection *DSProviderConnection1;
	TClientDataSet *csdCategory;
	TClientDataSet *csdService;
	TIntegerField *csdCategoryID;
	TWideStringField *csdCategoryNAME;
	TIntegerField *csdServiceID;
	TIntegerField *csdServiceCATEGORY_ID;
	TWideStringField *csdServiceNAME;
	TSingleField *csdServicePRICE;
	TIntegerField *csdServiceDURATION;
private:	// User declarations
	bool FInstanceOwner;
	TdmdatabaseClient* FdmdatabaseClient;
	TdmdatabaseClient* GetdmdatabaseClient(void);
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
	__fastcall ~Tdm();
	__property bool InstanceOwner = {read=FInstanceOwner, write=FInstanceOwner};
	__property TdmdatabaseClient* dmdatabaseClient = {read=GetdmdatabaseClient, write=FdmdatabaseClient};
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;
//---------------------------------------------------------------------------
#endif
