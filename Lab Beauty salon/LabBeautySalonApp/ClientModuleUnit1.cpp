//----------------------------------------------------------------------------

#pragma hdrstop
#include <stdio.h>
#include <memory>

#include "ClientModuleUnit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma classgroup "FMX.Controls.TControl"
#pragma resource "*.dfm"
Tdm *dm;
//---------------------------------------------------------------------------
__fastcall Tdm::Tdm(TComponent* Owner)
	: TDataModule(Owner)
{
	FInstanceOwner = true;
}

__fastcall Tdm::~Tdm()
{
	delete FdmdatabaseClient;
}

TdmdatabaseClient* Tdm::GetdmdatabaseClient(void)
{
	if (FdmdatabaseClient == NULL)
	{
		SQLConnection1->Open();
		FdmdatabaseClient = new TdmdatabaseClient(SQLConnection1->DBXConnection, FInstanceOwner);
	}
	return FdmdatabaseClient;
};

