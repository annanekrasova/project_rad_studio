//---------------------------------------------------------------------------

#ifndef EffectsH
#define EffectsH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Effects.hpp>
#include <FMX.Filter.Effects.hpp>
#include <FMX.Objects.hpp>
#include <FMX.Types.hpp>
//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TImage *Image1;
	TImage *Image5;
	TBlurEffect *BlurEffect1;
	TText *Text1;
	TImage *Image2;
	TImage *Image3;
	TImage *Image4;
	TImage *Image6;
	TText *Text8;
	TBevelEffect *BevelEffect1;
	TText *Text3;
	TText *Text2;
	TBandsEffect *BandsEffect1;
	TInvertEffect *InvertEffect1;
	TText *Text4;
	TContrastEffect *ContrastEffect1;
	TText *Text6;
	TText *Text7;
	TWaveEffect *WaveEffect1;
private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
#endif
