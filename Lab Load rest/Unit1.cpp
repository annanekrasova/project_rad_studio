//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
	 void LoadResourseToImage (UnicodeString aResName,TBitmap* aResult)
	 {
			TResourceStream* x;
			x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
			try {
				aResult->LoadFromStream(x);
			}
			__finally {
				delete x;
			}

	 }
	 //
	 UnicodeString LoadResourseToText(UnicodeString aResName)
	 {
			TResourceStream* x;
			TStringStream* xSS;
			x = new TResourceStream((int)HInstance, aResName, RT_RCDATA);
			try {
					xSS = new TStringStream("", TEncoding::UTF8, true);
				 try {
					  xSS->LoadFromStream(x);
						return xSS->DataString;
				 }__finally {
					 delete xSS;
				 }
			}
			__finally {
				delete x;
			}
	 }
void __fastcall Tfm::Button1Click(TObject *Sender)
{
	LoadResourseToImage("JpgImage_1",im->Bitmap);
	me->Lines->Text = LoadResourseToText("Resource_1");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button3Click(TObject *Sender)
{
LoadResourseToImage("JpgImage_2",im->Bitmap);
	me->Lines->Text = LoadResourseToText("Resource_1");
}
//---------------------------------------------------------------------------
void __fastcall Tfm::Button2Click(TObject *Sender)
{
	im->Bitmap->SetSize(0,0);
	me->Lines->Clear() ;
}
//---------------------------------------------------------------------------
