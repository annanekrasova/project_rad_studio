//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	tc->ActiveTab = tiList;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormShow(TObject *Sender)
{
	dm->FDConnection->Connected = true;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	dm->taNotes->Append();
    tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::lvClick(TObject *Sender)
{
	dm->taNotes->Edit();
    tc->GotoVisibleTab(tiItem->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button4Click(TObject *Sender)
{
	dm->taNotes->Post();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	dm->taNotes->Cancel();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button5Click(TObject *Sender)
{
	dm->taNotes->Delete();
	tc->GotoVisibleTab(tiList->Index);
}
//---------------------------------------------------------------------------
