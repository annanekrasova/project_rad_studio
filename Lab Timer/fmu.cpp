//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"

Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buRestartClick(TObject *Sender)
{
	FTimeValue = Time().Val + (double)30/(24*60*60);
	FTimePause = false;
	tm->Enabled = true;
}
//---------------------------------------------------------------------------
void __fastcall Tfm::buPauseClick(TObject *Sender)
{
if (FTimePause) {
	FTimeValue = Time().Val + FTimeValue;
	FTimePause = false;
	tm->Enabled = true;
	buPause->Text = L"Pause";
	}
else {
	tm->Enabled = false;
	FTimePause = true;
	FTimeValue = FTimeValue - Time().Val;
	buPause->Text = L"CONTINUE";
	}

}
//---------------------------------------------------------------------------
void __fastcall Tfm::buDel10secClick(TObject *Sender)
{
	FTimeValue = FTimeValue - (double)10/(24*60*60);
}
//---------------------------------------------------------------------------
void __fastcall Tfm::tmTimer(TObject *Sender)
{
	double x = FTimeValue - Time().Val;
	if (x<=0) {
		x=0;
		tm->Enabled = false;
	}
	laTime->Text=FormatDateTime("nn:ss",x);
   //	laTime->Text=FormatDateTime("nn:ss",x);
}
//---------------------------------------------------------------------------
