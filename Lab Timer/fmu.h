//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Graphics.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <FMX.Objects.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TButton *buDel10sec;
	TButton *buPause;
	TButton *buRestart;
	TLabel *laTime;
	TText *VGFG;
	TTimer *tm;
	void __fastcall buRestartClick(TObject *Sender);
	void __fastcall buPauseClick(TObject *Sender);
	void __fastcall buDel10secClick(TObject *Sender);
	void __fastcall tmTimer(TObject *Sender);
private:	// User declarations
	double FTimeValue;
	bool FTimePause;
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
