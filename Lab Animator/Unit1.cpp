//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
	Label1->Position->X= - Label1->Width;
	Label2->Position->X=this->Width + Label2->Width;
	Label3->Position->X= - Label3->Width;
	TAnimator::AnimateIntWait(Label1,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Label2,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);
	TAnimator::AnimateIntWait(Label3,"Position.X",0,1,TAnimationType::Out, TInterpolationType::Back);

}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender)
{
	Label4->AutoSize = true;
	Label4->TextSettings ->Font->Size = 400;
	TAnimator::AnimateIntWait(Label4,"TextSettings.Font.Size",96,1,TAnimationType::Out, TInterpolationType::Linear);
	Label4->AutoSize=false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button3Click(TObject *Sender)
{
	int y = Image1->Position->Y;
	Image1->Position->Y = - Image1->Height;
    TAnimator::AnimateIntWait(Image1,"Position.Y",y,1,TAnimationType::Out, TInterpolationType::Back);

}
//---------------------------------------------------------------------------
