//---------------------------------------------------------------------------

#ifndef FmuH
#define FmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class TForm1 : public TForm
{
__published:	// IDE-managed Components
	TTabControl *TC;
	TText *Text1;
	TButton *BuStart;
	TTabItem *TabItem1;
	TTabItem *TabItem2;
	TTabItem *TabItem3;
	TTabItem *TabItem4;
	TTabItem *TabItem5;
	TButton *Button2;
	TButton *Button1;
	TButton *Button3;
	TButton *Button4;
	TLabel *Label1;
	TLabel *Label2;
	TButton *Button5;
	TButton *Button6;
	TButton *Button7;
	TButton *Button8;
	TLabel *Label3;
	TButton *Button9;
	TButton *Button10;
	TButton *Button11;
	TButton *Button12;
	TButton *ButtonRestart;
	TLabel *Label5;
	TMemo *Memo1;
	TProgressBar *pb;
	TStyleBook *StyleBook1;
	TImage *Image1;
	void __fastcall ButtonAllClick(TObject *Sender);
	void __fastcall BuStartClick(TObject *Sender);
	void __fastcall ButtonRestartClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall TCChange(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm1(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm1 *Form1;
//---------------------------------------------------------------------------
#endif
