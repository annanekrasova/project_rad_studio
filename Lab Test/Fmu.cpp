//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Fmu.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonAllClick(TObject *Sender)
{
	Memo1->Lines->Add(
	L"Question" + TC->ActiveTab->Text + L"-" +
	((((TControl *)Sender)->Tag == 1) ? L"Right!": L"Wrong!")
	);
	TC->Next();
}

//---------------------------------------------------------------------------
void __fastcall TForm1::BuStartClick(TObject *Sender)
{
	Memo1->Lines->Clear();
	TC->Next();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::ButtonRestartClick(TObject *Sender)
{
	TC->ActiveTab=TabItem1;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::FormCreate(TObject *Sender)
{
	pb->Max = TC->TabCount-1;
	ButtonRestartClick(ButtonRestart);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::TCChange(TObject *Sender)
{
	pb->Value = TC->ActiveTab->Index;
}
//---------------------------------------------------------------------------

//---------------------------------------------------------------------------
