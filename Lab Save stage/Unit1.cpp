//---------------------------------------------------------------------------

#include <fmx.h>
#pragma hdrstop

#include "Unit1.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.fmx"
Tfm *fm;
//---------------------------------------------------------------------------
__fastcall Tfm::Tfm(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormCreate(TObject *Sender)
{
	 TBinaryReader* x;
	 if (SaveState->Stream->Size>0) {
		x=new TBinaryReader (SaveState->Stream, TEncoding::UTF8, false);
		try {
			Edit1->Text = x->ReadString();
			Edit2->Text = x->ReadString();
			DateEdit1->Text = x->ReadDouble();
			TabControl1->ActiveTab = TabControl1->Tabs [x->ReadInteger()];
		}
		__finally {x->DisposeOf();
		}

	 }
}
//---------------------------------------------------------------------------
void __fastcall Tfm::FormSaveState(TObject *Sender)

{       TBinaryWriter* x;
		SaveState->Stream->Clear();
		x=new TBinaryWriter (SaveState->Stream);
		try {
			x-> Write (Edit1->Text);
			x-> Write (Edit2->Text);
			x-> Write (DateEdit1->Date.Val);
			x->Write (TabControl1->ActiveTab->Index);
		}
		__finally {x->DisposeOf();
		}


}
//---------------------------------------------------------------------------
