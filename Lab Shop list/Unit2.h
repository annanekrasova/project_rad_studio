//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Data.DB.hpp>
#include <FireDAC.Comp.Client.hpp>
#include <FireDAC.Comp.DataSet.hpp>
#include <FireDAC.DApt.Intf.hpp>
#include <FireDAC.DatS.hpp>
#include <FireDAC.Phys.Intf.hpp>
#include <FireDAC.Stan.Error.hpp>
#include <FireDAC.Stan.Intf.hpp>
#include <FireDAC.Stan.Option.hpp>
#include <FireDAC.Stan.Param.hpp>
#include <FireDAC.Stan.StorageJSON.hpp>
#include <FMX.ImgList.hpp>
#include <System.ImageList.hpp>
#include <System.IOUtils.hpp>

//---------------------------------------------------------------------------
class Tdm : public TDataModule
{
__published:	// IDE-managed Components
	TFDMemTable *taList;
	TImageList *i1;
	TFDStanStorageJSONLink *FDStanStorageJSONLink1;
	TStringField *taListDetail;
	TIntegerField *taListImageIndex;
	TBooleanField *taListCheckmark;
	TStringField *taListHeaderText;
	TStringField *taListText;
	void __fastcall DataModuleCreate(TObject *Sender);
	void __fastcall taListAfterInsert(TDataSet *DataSet);
	void __fastcall taListAfterPost(TDataSet *DataSet);
private:
	 UnicodeString FFileName;
// User declarations
public:		// User declarations
	__fastcall Tdm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tdm *dm;

	const UnicodeString cNameDB = "labShoppingList.json";
//---------------------------------------------------------------------------
#endif
