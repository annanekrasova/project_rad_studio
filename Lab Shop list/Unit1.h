//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.ImgList.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.ListBox.hpp>
#include <FMX.ListView.Adapters.Base.hpp>
#include <FMX.ListView.Appearances.hpp>
#include <FMX.ListView.hpp>
#include <FMX.ListView.Types.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.TabControl.hpp>
#include <FMX.Types.hpp>
#include <Data.Bind.Components.hpp>
#include <Data.Bind.DBScope.hpp>
#include <Data.Bind.EngExt.hpp>
#include <Fmx.Bind.DBEngExt.hpp>
#include <Fmx.Bind.Editors.hpp>
#include <System.Bindings.Outputs.hpp>
#include <System.Rtti.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TTabControl *tc;
	TTabItem *tiItem;
	TListView *lv;
	TToolBar *tbList;
	TTabItem *tiList;
	TSpeedButton *sbEdit;
	TSpeedButton *sbCheck;
	TLabel *laCaption;
	TButton *buAdd;
	TButton *buPopup;
	TGridPanelLayout *GridPanelLayout1;
	TButton *buSave;
	TButton *buCancel;
	TScrollBox *ScrollBox1;
	TEdit *edText;
	TComboBox *cbHeaderText;
	TLabel *Label1;
	TLabel *Label2;
	TLabel *Label3;
	TToolBar *ToolBar1;
	TLabel *Label4;
	TLabel *Label5;
	TMemo *meDetail;
	TSwitch *swCheckmark;
	TLabel *Label6;
	TButton *buBack;
	TButton *buDel;
	TLabel *Label7;
	TButton *buImagePrev;
	TLayout *Layout1;
	TButton *buImageClear;
	TButton *buImageNext;
	TGlyph *Glyph1;
	TGridPanelLayout *GridPanelLayout3;
	TBindSourceDB *BindSourceDB1;
	TBindingsList *BindingsList1;
	TLinkListControlToField *LinkListControlToField1;
	TLinkControlToField *LinkControlToField2;
	TLinkFillControlToField *LinkFillControlToField1;
	TLinkControlToField *LinkControlToField3;
	TLinkControlToField *LinkControlToField1;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall buSaveClick(TObject *Sender);
	void __fastcall lvItemClick(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall lvUpdateObjects(TObject * const Sender, TListViewItem * const AItem);
	void __fastcall buAddClick(TObject *Sender);
	void __fastcall buBackClick(TObject *Sender);
	void __fastcall buDelClick(TObject *Sender);
	void __fastcall buCancelClick(TObject *Sender);
	void __fastcall buImagePrevClick(TObject *Sender);
	void __fastcall buImageNextClick(TObject *Sender);
	void __fastcall buImageClearClick(TObject *Sender);
	void __fastcall Glyph1Changed(TObject *Sender);


private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
