//---------------------------------------------------------------------------

#ifndef fmuH
#define fmuH
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <FMX.Controls.hpp>
#include <FMX.Forms.hpp>
#include <FMX.ActnList.hpp>
#include <FMX.Controls.Presentation.hpp>
#include <FMX.Edit.hpp>
#include <FMX.Layouts.hpp>
#include <FMX.Objects.hpp>
#include <FMX.StdCtrls.hpp>
#include <FMX.Types.hpp>
#include <IdBaseComponent.hpp>
#include <IdComponent.hpp>
#include <IdTCPClient.hpp>
#include <IdTCPConnection.hpp>
#include <System.Actions.hpp>
#include <FMX.Memo.hpp>
#include <FMX.ScrollBox.hpp>
//---------------------------------------------------------------------------
class Tfm : public TForm
{
__published:	// IDE-managed Components
	TLayout *Layout1;
	TButton *buDissconnect;
	TButton *buImage;
	TButton *buStr;
	TButton *buTime;
	TButton *buConnect;
	TImage *im;
	TEdit *edPort;
	TEdit *edHost;
	TActionList *ActionList1;
	TIdTCPClient *IdTCPClient;
	TAction *acConnect;
	TAction *acDisconnect;
	TAction *acGetTime;
	TAction *acGetStr;
	TAction *acGetImage;
	TMemo *me;
	void __fastcall acConnectExecute(TObject *Sender);
	void __fastcall acDisconnectExecute(TObject *Sender);
	void __fastcall acGetTimeExecute(TObject *Sender);
	void __fastcall acGetStrExecute(TObject *Sender);
	void __fastcall acGetImageExecute(TObject *Sender);
	void __fastcall ActionList1Update(TBasicAction *Action, bool &Handled);
private:	// User declarations
public:		// User declarations
	__fastcall Tfm(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfm *fm;
//---------------------------------------------------------------------------
#endif
